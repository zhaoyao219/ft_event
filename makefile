CC := gcc
CXX := g++

BASEDIR = $(shell pwd)


MAXDEPTH = 1

#list all source files include *.cpp *.cc *.c
ALL_SRC := $(shell find -E $(BASEDIR) -maxdepth $(MAXDEPTH) -regex ".*\.(cpp|c|cc)")

#replace *.cpp -> *.o, *.c -> *.o *.cc -> *.o
OBJS := $(patsubst %.c,%.o,$(ALL_SRC))
OBJS += $(patsubst %.cpp,%.o,$(ALL_SRC))
OBJS += $(patsubst %.cc,%.o,$(ALL_SRC))

#filter all *.o
OBJS := $(filter %.o, $(OBJS))

CFLAGS=-m64 -Wall -g -fPIC
INC := -I $(BASEDIR) 


TARGET=test


$(TARGET): $(OBJS)
	$(CXX) -o $@ $(INC) $(CFLAGS) $^ 


%.o: %.cpp
	$(CXX) $(CFLAGS) $(INC) -c -o $@ $<
%.o: %.c
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<
%.o: %.cc
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<


clean:
	rm -f $(OBJS)
	rm -f $(TARGET)
